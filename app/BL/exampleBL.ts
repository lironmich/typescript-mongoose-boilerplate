import { BaseBL } from "./baseBL";
import { IExample, exampleSchema } from "../models/example";
import { Document, Model, model } from "mongoose";

class ExampleBL extends BaseBL<IExample & Document> {
    protected get collection(): Model<IExample & Document> {
        return model('Example', exampleSchema);
    }
}

export const exampleBL = new ExampleBL();