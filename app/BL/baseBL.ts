import { IModel } from "../models/model";
import { Document, Model } from "mongoose";

export abstract class BaseBL<T extends IModel> {
    protected abstract get collection(): Model<T & Document>;

    public get(filter?: Object): Promise<T[]> {
        return this.collection.find(filter || {}).exec();
    }

    public getById(id: string): Promise<T | null> {
        return this.collection.findById(id).exec();
    }

    public first(filter?: Object): Promise<T | null> {
        return this.collection.findOne(filter || {}).exec();
    }

    public create(model: T): Promise<T> {
        return this.collection.create(model);
    }

    public update(model: T): Promise<T> {
        return this.collection.update({ _id: model.id}, model).exec();
    }

    public delete(model: T): Promise<void> {
        return this.collection.remove({ _id: model.id}).exec().then(_ => {});
    }
}