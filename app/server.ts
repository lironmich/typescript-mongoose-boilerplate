import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as mongoose from 'mongoose';
import { configurationManager } from "./common/configuration-manager/configuration-manager";
import { logger } from './common/logger/logger';
import { setupRoutes } from './routes/routes';

process.chdir(__dirname);
configurationManager.environment = (<any>process.env.node_env) || 'development';

const app = express();
const router = express.Router();
const port = configurationManager.port;

app.use(router);
app.use(bodyParser.json({ limit: '10mb'}));
app.use(bodyParser.urlencoded({ extended: false}));

mongoose.connect(configurationManager.mongoDB, {useNewUrlParser: true }, (err) => {
    if (err) {
        logger.error('Failed logging to mongoDB');
        return;
    }

    setupRoutes(router);

    app.listen(port, () => {
        logger.info(`Express server listening or port ${port} in ${configurationManager.environment} mode`);
    });
})