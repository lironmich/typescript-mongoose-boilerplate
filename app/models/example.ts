import { IModel } from "./model";
import { Schema } from "mongoose";

export interface IExample extends IModel {
    name: string;
}

export const exampleSchema = new Schema({
    name: String
});