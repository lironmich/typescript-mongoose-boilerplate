import { config } from '../../server-config';

type Environment = 'development' | 'production' | 'testing';

interface IConfigurationManager {
    name: string;
    port: string;
    environment: Environment;
    isDevelopment: boolean;
    isProduction: boolean;
    isTesting: boolean;
    mongoDB: string;
}

class ConfigurationManager implements IConfigurationManager {
    private _environment: Environment;

    public get name(): string {
        return config.app.name;
    }

    public get port(): string {
        return config.app.port;
    }

    public get environment(): Environment {
        return this._environment;
    }

    public set environment(value: Environment) {
        if (value !== 'development' &&
            value !== 'production' &&
            value !== 'testing') {
                throw new Error('Invalid environment');
            }

        this._environment = value;
    }

    public get isDevelopment(): boolean {
        return this._environment === 'development';
    }

    public get isProduction(): boolean {
        return this._environment === 'production';
    }

    public get isTesting(): boolean {
        return this._environment == 'testing';
    }

    public get mongoDB(): string {
        return config.db.mongoDB;
    }
}

export const configurationManager: IConfigurationManager = new ConfigurationManager();