import { expect } from 'chai';
import { configurationManager } from './configuration-manager';

describe('configuration-manager', () => {
    describe('environment', () => {
        it('by default should be testing', (done) => {
            expect(configurationManager.environment).to.be.equals('testing');
            done();
        });

        it('setting invalid environment should throw exception', (done) => {
            expect(() => {
                configurationManager.environment = <any>'invalid';
            }).to.throw();

            done();
        });
    });
});