import { Response } from "express";

export function notFound(res: Response, err: any) {
    return res.status(404).send(err);
}

export function failure(res: Response, err: any) {
    res.status(400).send(err);
}