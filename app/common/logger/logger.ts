import * as winston from 'winston';
import { configurationManager } from '../configuration-manager/configuration-manager';

interface ILogger {
    info(message: string): void;
    warning(message: string): void;
    error(message: string): void;
}

class Logger implements ILogger {
    private logger: winston.LoggerInstance;

    constructor() {
        this.logger = new winston.Logger({
            transports: [
                new winston.transports.Console(),
                new winston.transports.File({filename: configurationManager.name })
            ]
        });
    }

    public info(message: string): void {
        this.executeLog(this.logger.info, message);
    }

    public warning(message: string): void {
        this.executeLog(this.logger.warning, message);
    }

    public error(message: string): void {
        this.executeLog(this.logger.error, message);
    }

    private executeLog(logMethod: winston.LeveledLogMethod, message: string) {
        if (configurationManager.isTesting) {
            return;
        }

        logMethod(message);
    }
}

export const logger: ILogger = new Logger();