import { Router, Request, Response } from "express";
import { IModel } from "../models/model";
import { BaseBL } from "../BL/baseBL";
import { notFound } from "../common/http-utils/http-utils";

export class BaseRoute {

    constructor(router: Router,
        protected BL: BaseBL<IModel>,
        modelName: string) {

        const basePath = `/api/${modelName}`;

        router.get(basePath, this.getDocuments.bind(this));
        router.get(`${basePath}/create`, this.createDocument.bind(this));
        router.get(`${basePath}/update`, this.updateDocument.bind(this));
        router.get(`${basePath}/delete`, this.deleteDocument.bind(this));
        router.get(`${basePath}/:id`, this.getDocumentById.bind(this));
    }

    private getDocuments(req: Request, res: Response): void {
        const filter: Object = JSON.parse(req.query.filter || '{}');

        this.BL.get(filter).then(model => {
            res.json(model)
        }, () => {
            notFound(res, 'Failed in getting documents');
        })
    }

    private getDocumentById(req: Request, res: Response): void {
        const id: string = req.params.id;

        this.BL.getById(id).then(model => {
            res.json(model)
        }, () => {
            notFound(res, `Failed in getting document with id ${id}`);
        })
    }

    private createDocument(req: Request, res: Response): void {
        if (!req.query.document) {
            notFound(res, 'Failed creating document, no document sent');
            return;
        }

        const document: IModel = JSON.parse(req.query.document);

        this.BL.create(document).then((model) => {
            res.json(model);
        }, err => {
            notFound(res, `Faied creating document, ${err.message}`);
        });
    }

    private updateDocument(req: Request, res: Response): void {
        if (!req.query.document) {
            notFound(res, 'Failed updating document, no document sent');
            return;
        }

        const document: IModel = JSON.parse(req.query.document);
        if (!document.id) {
            notFound(res, 'Failed updating document, no document id');
            return;
        }

        this.BL.update(document).then((model) => {
            res.json(model);
        }, err => {
            notFound(res, `Faied updating document, ${err.message}`);
        });
    }

    private deleteDocument(req: Request, res: Response): void {
        if (!req.query.document) {
            notFound(res, 'Failed deleting document, no document sent');
            return;
        }

        const document: IModel = JSON.parse(req.query.document);
        if (!document.id) {
            notFound(res, 'Failed deleting document, no document id');
            return;
        }

        this.BL.delete(document).then((model) => {
            res.json(model);
        }, err => {
            notFound(res, `Faied deleting document, ${err.message}`);
        });
    }
}