import { Router } from "express";
import { ExampleRouter } from "./exampleRouter";

export function setupRoutes(router: Router) {
    new ExampleRouter(router);
}