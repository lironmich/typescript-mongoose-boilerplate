import { Router } from 'express'
import { BaseRoute } from './baseRoute';
import { exampleBL } from '../BL/exampleBL';

export class ExampleRouter extends BaseRoute {
    constructor(router: Router) {
        super(router, exampleBL, 'example');
    }
}