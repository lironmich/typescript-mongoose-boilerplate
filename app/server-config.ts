interface IApplicationConfiguration {
    name: string;
    port: string;
}

interface IDBConfiguration {
    mongoDB: string;
}

interface ILoggerConfiguration {
    apiFileLocation: string;
    exceptionsFileLocation: string;
}

interface IServerConfiguration {
    app: IApplicationConfiguration;
    db: IDBConfiguration;
    logger: ILoggerConfiguration;
}

const config: IServerConfiguration = {
    app: {
        name: "typescript-mongoose-boilerplate",
        port: process.env.port || '3000'
    },
    db: {
        mongoDB: "mongodb://user1:password1@ds117156.mlab.com:17156/example"
    },
    logger: {
        apiFileLocation: "logs/api.log",
        exceptionsFileLocation: "logs/exceptions.log"
    }
}

const serverUrl = `http://localhost:${config.app.port}`;

export {config, serverUrl};